package com.beta.htc.load.service;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.beta.htc.load.dto.Container;
import com.beta.htc.load.dto.OpError;
import com.beta.htc.load.dto.DetailRequest;
import com.beta.htc.load.dto.PaysheetRequest;
import com.beta.htc.load.exception.AbstractException;
import com.beta.htc.load.exception.InconsistentCompany;
import com.beta.htc.load.exception.InconsistentDate;
import com.beta.htc.load.hystrix.OperationsServiceHystrix;
import com.beta.htc.load.util.AppConst;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;

@Service
public class UploadService {

	private final Logger log = LoggerFactory.getLogger(getClass());
	private static final String UPLOAD_SERVICE 	= "Upload-Service";
	
	private static final String NAME_PATTERN 	= "PRICELIST_";
	
	
	@Autowired
	OperationsServiceHystrix operationsServiceHystrix;
	
	public ResponseEntity<List<Container>> processMultipleCsv(MultipartFile[] files) {
		
		HttpHeaders headers 				= new HttpHeaders();
		List<Container> containers 			= new ArrayList<>();
		List<Container> requests 			= new ArrayList<>();

		for (int i = 0; i < files.length; i++) {
			Container c = parseCsv(files[i]);
			if (!c.getPaysheetOk()) {
				c.setCode(AppConst.CODE_BAD_REQUEST);
				c.setDescription(AppConst.MSG_BAD_REQUEST);
				containers.add(c);
			} else {
				requests.add(c);
			}
		}
		// si no hay resultados correctos se retorna los contendores
		if (requests.isEmpty()) {
			headers.add(UPLOAD_SERVICE, "No hay nominas con datos correctas");
			return new ResponseEntity<>(containers,headers,HttpStatus.BAD_REQUEST);
		}else if(!containers.isEmpty()){
			headers.add(UPLOAD_SERVICE, "Hay nominas con datos erroneos pero tambien correctas");
		}else {
			headers.add(UPLOAD_SERVICE, "Todas las nominas se cargaron correctamente");
		}
		// contenedor de nominas ordenadas por fechas, de la ultima a la mas reciente
		requests.sort((d1, d2) -> d1.getPaysheet().getCreated().compareTo(d2.getPaysheet().getCreated()));
		
		List<Container> responses = operationsServiceHystrix.savePaysheets(requests, headers);
		containers.addAll(responses);
		//HttpStatus status = HttpStatus.OK;
		//if(responses.stream().anyMatch( r->r.getCode()==AppConst.CODE_SERVICE_SAVE_UNAVAILABLE ))
		//	status = HttpStatus.SERVICE_UNAVAILABLE;
		
		return new ResponseEntity<>(containers,headers,HttpStatus.OK);
	}
	
	

	private Container parseCsv(MultipartFile file) {
		if (file == null)
			return new Container(new OpError(AppConst.CODE_UPLOAD_NULL, 0, AppConst.MSG_UPLOAD_NULL));
		String fileName = "unknow";
		try {
			fileName = file.getOriginalFilename();
			Container container = new Container(fileName);

			if (!fileName.split("\\.")[1].equalsIgnoreCase("CSV"))
				return new Container(fileName, new OpError(AppConst.CODE_CSV_FILE, 0, AppConst.MSG_CSV_FILE));
			if (!fileName.contains(NAME_PATTERN))
				return new Container(fileName, new OpError(AppConst.CODE_INVALID_PATTERN, 0, AppConst.MSG_INVALID_PATTERN));

			InputStream input = file.getInputStream();
			CsvMapper mapper = new CsvMapper();
			mapper.enable(CsvParser.Feature.WRAP_AS_ARRAY);
			MappingIterator<String[]> it = mapper.readerFor(String[].class).readValues(input);

			Timestamp time=getTimestampFileName(fileName,  AppConst.PATTERN_DATE_FILE );
			if (time == null) 
				return new Container(fileName, new OpError(AppConst.CODE_INVALID_DATE, 0, AppConst.MSG_INVALID_DATE));
			//creando la factura
			container.setPaysheet(new PaysheetRequest(time));
			parseLine(it, container, true);// obtiene informacion de cabecera
			while (it.hasNext()) parseLine(it, container, false); //recorre los demas campos
			return container;
		} catch (IOException e) {
			log.error(e.getMessage());
			return new Container(fileName, new OpError(AppConst.CODE_OPEN_FILE, 0, AppConst.MSG_OPEN_FILE));
		} catch (Exception e) {
			log.error(e.getMessage());
			return new Container(fileName, new OpError(AppConst.CODE_PROCESS_FILE, 0, AppConst.MSG_PROCESS_FILE));
		}
	}

	private void parseLine(MappingIterator<String[]> it, Container container, boolean setPaysheetHeaders) {
		Integer fileLineNumber = it.getCurrentLocation().getLineNr();
		String[] row = it.next();
		try {
			if (setPaysheetHeaders)
				container.getPaysheet().setCompany(Integer.valueOf(row[1]));
			Date date = new SimpleDateFormat(AppConst.PATTERN_DATE_COLUMN).parse(row[0]);
			if (!container.getPaysheet().getCreated().equals(new Timestamp(date.getTime())))
				throw new InconsistentDate(fileLineNumber);
			if (!container.getPaysheet().getCompany().equals(Integer.parseInt(row[1])))
				throw new InconsistentCompany(fileLineNumber);
			//guardando los demas datos del detalle
			container.getPaysheet().getDetails().add(parseDetail(row));
		} catch (AbstractException e) {
			container.addError(new OpError(e.getCode(), fileLineNumber, e.getDescription()));
			log.error(e.toString());
		} catch (NumberFormatException e) {
			container.addError(new OpError(AppConst.CODE_PARSE_NUMBER, fileLineNumber, AppConst.MSG_PARSE_NUMBER));
			log.error(e.toString());
		} catch (ArrayIndexOutOfBoundsException e) {
			container.addError(new OpError(AppConst.CODE_OUT_OF_BOUNDS, fileLineNumber, AppConst.MSG_OUT_OF_BOUNDS));
			log.error(e.getMessage());
		} catch (ParseException e) {
			container.addError(new OpError(AppConst.CODE_PARSE_EXCEPTION, fileLineNumber, AppConst.MSG_PARSE_EXCEPTION));
			log.error(e.getMessage());
		} catch (Exception e) {
			container.addError(new OpError(AppConst.CODE_ERROR_DATA, fileLineNumber,AppConst.MSG_ERROR_DATA + Arrays.toString(row)));
			log.error(e.getMessage());
		}
	}

	private DetailRequest parseDetail(String[] row) {
		DetailRequest detail = new DetailRequest();
		detail.setIdProduct(row[2]);
		detail.setUnitPrice(new BigDecimal(row[3]));
		detail.setQuantity(Integer.parseInt(row[4]));
		detail.setExempt(row[5].equalsIgnoreCase("T"));
		detail.setCesc(row[6].equalsIgnoreCase("T"));
		detail.setPromotion(Integer.parseInt(row[7]));
		return detail;
	}
	
	public Timestamp getTimestampFileName(String fileName,String pattern) {
		try {
			int start 		= fileName.indexOf('_')+1;
			int end 		= fileName.lastIndexOf('.');
			String rawDate 	= fileName.substring(start, end);
			Date date =  new SimpleDateFormat(pattern).parse(rawDate);
			return new Timestamp(date.getTime());
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}
	}	
}