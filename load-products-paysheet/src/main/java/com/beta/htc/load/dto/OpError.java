package com.beta.htc.load.dto;

public class OpError {
	private Long code;
	private Integer lineNumber;
	private String description;
	
	public OpError() {
		super();
	}
	
	public OpError(Long code,Integer lineNumber, String description) {
		super();
		this.code =code;
		this.lineNumber = lineNumber;
		this.description = description;
	}
	public Integer getLineNumber() {
		return lineNumber;
	}
	public void setLineNumber(Integer lineNumber) {
		this.lineNumber = lineNumber;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getCode() {
		return code;
	}
	public void setCode(Long code) {
		this.code = code;
	}
}
