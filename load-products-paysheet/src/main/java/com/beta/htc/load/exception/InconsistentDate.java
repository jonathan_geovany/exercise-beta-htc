package com.beta.htc.load.exception;

import com.beta.htc.load.util.AppConst;

public class InconsistentDate extends AbstractException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final long DEFAULT_CODE_EXCEPTION = AppConst.CODE_INCONSISTENT_DATE;
	private static final String DEFAULT_MSG_EXCEPTION = AppConst.MSG_INCONSISTENT_DATE;
	
	public InconsistentDate(Exception ex) {
		super(ex);
	}
	public InconsistentDate(long code, String description, Exception ex) {
		super(code, description, ex);
	}
	public InconsistentDate(long code, String description) {
		super(code, description);
	}
	public InconsistentDate() {
		super(DEFAULT_CODE_EXCEPTION, DEFAULT_MSG_EXCEPTION);
	}
	public InconsistentDate(Integer fileLineNumber) {
		super(DEFAULT_CODE_EXCEPTION, DEFAULT_MSG_EXCEPTION);
		setFileLineNumber(fileLineNumber);
	}
}
