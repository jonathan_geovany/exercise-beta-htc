package com.beta.htc.load.exception;

import com.beta.htc.load.util.AppConst;

public class InconsistentCompany extends AbstractException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final long DEFAULT_CODE_EXCEPTION = AppConst.CODE_INCONSISTENT_COMPANY;
	private static final String DEFAULT_MSG_EXCEPTION = AppConst.MSG_INCONSISTENT_COMPANY;
	
	public InconsistentCompany(Exception ex) {
		super(ex);
	}
	public InconsistentCompany(long code, String description, Exception ex) {
		super(code, description, ex);
	}
	public InconsistentCompany(long code, String description) {
		super(code, description);
	}
	public InconsistentCompany() {
		super(DEFAULT_CODE_EXCEPTION, DEFAULT_MSG_EXCEPTION);
	}
	public InconsistentCompany(Integer fileLineNumber) {
		super(DEFAULT_CODE_EXCEPTION, DEFAULT_MSG_EXCEPTION);
		setFileLineNumber(fileLineNumber);
	}
}
