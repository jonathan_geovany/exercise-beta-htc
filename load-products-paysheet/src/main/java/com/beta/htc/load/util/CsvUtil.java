package com.beta.htc.load.util;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beta.htc.load.dto.OpError;
import com.beta.htc.load.dto.ResultParse;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;


public class CsvUtil {
	
	private final Logger log = LoggerFactory.getLogger(getClass());
	private String fileName;
	private InputStream input;
	private boolean hasError;
	private ResultParse resultParse;
	
	public CsvUtil(String fileName, InputStream input) {
		super();
		this.fileName 	= fileName;
		this.input 		= input;
		hasError = false;
	}
	
	public boolean isValidFileName(String pattern,String extension){
		try {
			String[] elements = fileName.split("\\.");
			boolean isValid = elements[0].matches(pattern) && elements[1].equalsIgnoreCase(extension);
			if (!isValid) resultParse.getErrors().add(new OpError(AppConst.CODE_INVALID_PATTERN, 0, AppConst.MSG_INVALID_PATTERN));
			return hasError;
		} catch (Exception e) {
			log.error(e.getMessage());
			resultParse.getErrors().add(new OpError(AppConst.CODE_INVALID_PATTERN, 0, AppConst.MSG_INVALID_PATTERN));
			return false;
		}
	}
	
	public ResultParse evaluateCsv(List<ColumnDefinition> definitions){
		
		CsvMapper mapper = new CsvMapper();
		List<Map<String, Object>> parsedLines 	= new ArrayList<>();
		List<OpError> errors					= new ArrayList<>();
		mapper.enable(CsvParser.Feature.WRAP_AS_ARRAY);
		MappingIterator<String[]> it;
		try {
			it = mapper.readerFor(String[].class).readValues(input);
		} catch (IOException e) {
			log.error(e.getMessage());
			errors.add(new OpError(AppConst.CODE_OPEN_FILE, 0, AppConst.MSG_OPEN_FILE));	
			return new ResultParse(Collections.emptyList(), errors);
		}
		Integer fileLineNumber 	= 0;
		while (it.hasNext()) {
			String[] row 	= it.next();
			Map<String, Object> line = new HashMap<>();
			fileLineNumber 	= it.getCurrentLocation().getLineNr();
			
			for (int i=0;i<definitions.size();i++) {
				try {
					if(definitions.get(i).getType()== Integer.class ) {
						line.put(definitions.get(i).getName() , Integer.parseInt(row[i]));
					}
					if(definitions.get(i).getType()== Double.class ) {
						line.put(definitions.get(i).getName(), Double.parseDouble(row[i]));
					}
					if(definitions.get(i).getType()== BigDecimal.class ) {
						line.put(definitions.get(i).getName(),  new BigDecimal(row[i]));
					}
					if(definitions.get(i).getType()== String.class ) {
						line.put(definitions.get(i).getName(), row[i]);
					}
					if(definitions.get(i).getType()== Boolean.class ) {
						if(definitions.get(i).getFormat()!=null)
							line.put(definitions.get(i).getName(),Boolean.valueOf(row[i].matches(definitions.get(i).getFormat())));
						else
							line.put(definitions.get(i).getName(),Boolean.valueOf(row[i]));
					}
					if(definitions.get(i).getType()== Timestamp.class ) {
						Date date = new SimpleDateFormat(definitions.get(i).getFormat()).parse(row[i]);
						line.put(definitions.get(i).getName(), new Timestamp(date.getTime()));
					}
					//agregar mas validaciones aqui...
				}catch (ArrayIndexOutOfBoundsException e) {
					errors.add(new OpError(AppConst.CODE_OUT_OF_BOUNDS, fileLineNumber, AppConst.MSG_OUT_OF_BOUNDS));
					log.error(e.getMessage());
				} 
				catch (NumberFormatException e) {
					errors.add(new OpError(AppConst.CODE_PARSE_NUMBER, fileLineNumber, AppConst.MSG_PARSE_NUMBER));
					log.error(e.getMessage());
				}
				catch (ParseException e) {
					errors.add(new OpError(AppConst.CODE_PARSE_EXCEPTION, fileLineNumber, AppConst.MSG_PARSE_EXCEPTION+row[i]));
					log.error(e.getMessage());
				}
				catch (Exception e) {
					errors.add(new OpError(AppConst.CODE_PARSE_EXCEPTION, fileLineNumber, AppConst.MSG_PARSE_EXCEPTION));
					log.error(e.getMessage());
				}
			}
			parsedLines.add(line);
		}
		resultParse = new ResultParse(parsedLines,errors);
		return resultParse;
	}
	
	
	public List<OpError> columnHasConsistency(Object[] headers,String[] columns) {
		List<OpError> errors = new ArrayList<>();
		if(resultParse==null) resultParse = new ResultParse(fileName);
		try {
			if(columns==null) return Collections.emptyList();
			if(headers==null) headers = new Object[columns.length];
			for (int i = 0; i < columns.length; i++) if(headers[i]==null) headers[i] = resultParse.getValues().get(0).get(columns[i]);
			for (int i = 0; i < resultParse.getValues().size(); i++) {
				for (int j = 0; j < columns.length; j++) if(!resultParse.getValues().get(i).get(columns[j]).equals(headers[j])) errors.add(new OpError(AppConst.CODE_INCONSISTENT_DATE, i, AppConst.MSG_INCONSISTENT_DATE+resultParse.getValues().get(i).get(columns[j])));
			}
		}catch (Exception e) {
			log.error(e.getMessage());
		}
		resultParse.getErrors().addAll(errors);
		return errors;
	}
	
	public List<Map<String, Object>> filterMapList(String[] keys){
		return resultParse.getValues().stream().map(i->{
			Map<String,Object> rs = new HashMap<>();
			for (String key : keys) {
				if(i.containsKey(key)) rs.put(key, i.get(key));
			}
			return rs;
		}).collect(Collectors.toList());
	}

	
	public Timestamp getTimestampFileName(String pattern) {
		try {
			int start 		= fileName.indexOf('_');
			int end 		= fileName.lastIndexOf('.');
			String rawDate 	= fileName.substring(start, end);
			Date date =  new SimpleDateFormat(pattern).parse(rawDate);
			return new Timestamp(date.getTime());
		} catch (Exception e) {
			resultParse.getErrors().add(new OpError(AppConst.CODE_INVALID_DATE, 0, AppConst.MSG_INVALID_DATE));
			log.error(e.getMessage());
			return null;
		}
	}
	
	public ResultParse getResultParse() {
		return resultParse;
	}
	
	
	
}
