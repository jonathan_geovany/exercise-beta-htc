package com.beta.htc.load.client;

import java.util.List;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.beta.htc.load.dto.Container;
import com.beta.htc.load.dto.PaysheetRequest;


@FeignClient(name="zuul-getaway-server",path="operations-products-paysheet")
@RibbonClient(name="operations-products-paysheet")
public interface OperationsServiceClient {
	
	@PostMapping(value="/save-multiple",produces= MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE )
	List<Container> saveMultiplePaysheets(@RequestBody List<PaysheetRequest> paysheets);
	
	@PostMapping(value="/test")
	public String test(@RequestBody String msg);
}