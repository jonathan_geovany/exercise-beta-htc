package com.beta.htc.load.hystrix;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.beta.htc.load.client.OperationsServiceClient;
import com.beta.htc.load.dto.Container;
import com.beta.htc.load.dto.PaysheetRequest;
import com.beta.htc.load.util.AppConst;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Component
public class OperationsServiceHystrix {

	private final Logger log = LoggerFactory.getLogger(getClass());
	private static final String SAVE_SERVICE 	= "Save-Service";
	
	@Autowired
	private OperationsServiceClient operationsPaysheet;
	
	
	@HystrixCommand(fallbackMethod="testError")
	public String test(String msg) {
		msg+= " load service ";
		return operationsPaysheet.test(msg);
	}
	
	public String testError(String msg) {
		msg+=" Error de conexion ";
		return msg;
	}
	
	@HystrixCommand(fallbackMethod="getError")
	public List<Container> savePaysheets(List<Container> requests,HttpHeaders headers){
		List<PaysheetRequest> paysheets = requests.stream().map(Container::getPaysheet).collect(Collectors.toList());
		
		List<Container> responses = new ArrayList<>();
		responses.addAll(operationsPaysheet.saveMultiplePaysheets(paysheets));
		boolean allPaysheetsAreOk 		= responses.stream().allMatch(Container::getPaysheetOk);
		headers.add(SAVE_SERVICE, (allPaysheetsAreOk)?"Nominas correctas guardadas":"Nominas enviadas contenian errores");
		log.info("proceso de llamado de microservicio A correcto");
		return responses;
	}
	
	public List<Container> getError(List<Container> requests,HttpHeaders headers){
		log.error("llamando a metodo de falla de hystris");
		requests.stream().forEach(r->{
			r.setCode(AppConst.CODE_SERVICE_SAVE_UNAVAILABLE);
			r.setDescription(AppConst.MSG_SERVICE_SAVE_UNAVAILABLE);
		});
		headers.add(SAVE_SERVICE, "No se pudo conectar con el servicio de guardado");
		
		return requests;
	}
}
