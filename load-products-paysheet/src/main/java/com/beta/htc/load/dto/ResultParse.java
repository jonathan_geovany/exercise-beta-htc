package com.beta.htc.load.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.beta.htc.load.util.AppConst;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class ResultParse {
	private String fileName;
	private String description = AppConst.MSG_OK;
	private Long code = AppConst.CODE_OK;
	private List<Map<String, Object>> values;
	private List<OpError> errors;
	@JsonIgnore
	PaysheetRequest paysheetRequest;
	
	public ResultParse(String fileName) {
		super();
		this.fileName 	= fileName;
		this.values 	= new ArrayList<>();
		this.errors 	= new ArrayList<>();
	}

	public ResultParse(List<Map<String, Object>> values, List<OpError> errors) {
		super();
		this.values = values;
		this.errors = errors;
	}

	public List<Map<String, Object>> getValues() {
		return values;
	}

	public void setValues(List<Map<String, Object>> values) {
		this.values = values;
	}

	public List<OpError> getErrors() {
		return errors;
	}

	public void setErrors(List<OpError> errors) {
		this.errors = errors;
	}
	
	public boolean hasError() {
		return !this.errors.isEmpty();
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}
	
}
