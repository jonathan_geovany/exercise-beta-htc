package com.beta.htc.load.handler;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;




@ControllerAdvice
public class ResponseHttpExceptionHandler extends ResponseEntityExceptionHandler{
	
	private static final String INVALID_DATA_TYPE = "seleccione un archivo valido";
	private static final String INTERNAL_SERVER_ERROR = "Error al realizar proceso de carga de archivos";
	private static final String DESCRIPTION = "Description";
	private Logger log = LoggerFactory.getLogger(getClass());
	
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
		log.error(ex.getMessage());
		HttpHeaders headers = new HttpHeaders();
		headers.add(DESCRIPTION, INTERNAL_SERVER_ERROR);
		return new ResponseEntity<>(null,headers,HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@Override
	protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(
			HttpMediaTypeNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

		List<MediaType> mediaTypes = ex.getSupportedMediaTypes();
		if (!CollectionUtils.isEmpty(mediaTypes)) {
			headers.setAccept(mediaTypes);
		}
		headers.add(DESCRIPTION, INVALID_DATA_TYPE);
		return handleExceptionInternal(ex, null, headers, status, request);
	}
	
	@ExceptionHandler(MultipartException.class)
	public ResponseEntity<Object> handleMultipartException(MultipartException e){
		HttpHeaders headers = new HttpHeaders();
		headers.add(DESCRIPTION, INVALID_DATA_TYPE);
		return new ResponseEntity<>(null,headers,HttpStatus.BAD_REQUEST);
	}
	
	
	
}
