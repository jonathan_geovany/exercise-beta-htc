package com.beta.htc.load.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.beta.htc.load.dto.Container;
import com.beta.htc.load.hystrix.OperationsServiceHystrix;
import com.beta.htc.load.service.UploadService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@RestController
public class UploadController {
	
	@Autowired
	private UploadService uploadService;

	@Autowired
	private OperationsServiceHystrix calls;
	
	@HystrixCommand
	@PostMapping("/upload")
    public ResponseEntity<List<Container>> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
		return uploadService.processMultipleCsv(files);
	}
	
	@HystrixCommand
	@PostMapping("/test")
	public ResponseEntity<String> test(@RequestParam("msg") String msg){
		return new ResponseEntity<>(calls.test(msg), HttpStatus.OK);
	}
}
