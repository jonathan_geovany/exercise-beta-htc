package com.beta.htc.load.util;

public final class AppConst {
	
	private AppConst() {
		throw new IllegalStateException("AppConst class");
	}
	
	public static final String PATTERN_DATE_FILE 	= "yyyyMMdd_HH_mm";
	public static final String PATTERN_DATE_COLUMN 	= "dd/MM/yyyy HH:mm";
	public static final String PATTERN_FILE_NAME	= "PRICELIST_[0-9]{8}_[0-9]{1,2}_[0-9]{1,2}";
	
	public static final Long CODE_OK = 0L;
	public static final String MSG_OK = "Archivo sin errores de sintaxis";
	
	public static final Long CODE_BAD_REQUEST = 400L;
	public static final String MSG_BAD_REQUEST = "Errores en los parametros de entrada";
	
	
	public static final Long CODE_INCONSISTENT_COMPANY = 401L;
	public static final String MSG_INCONSISTENT_COMPANY = "Id de company es inconsistente en el archivo ";
	
	public static final Long CODE_INCONSISTENT_DATE = 402L;
	public static final String MSG_INCONSISTENT_DATE = "Fecha de nomina es inconsistente en el archivo ";
	
	public static final Long CODE_PARSE_EXCEPTION = 403L;
	public static final String MSG_PARSE_EXCEPTION = "No se pudo convertir el dato ";

	public static final Long CODE_UPLOAD_FILE = 404L;
	public static final String MSG_UPLOAD_FILE = "Error al cargar el archivo ";
	
	public static final Long CODE_PROCESS_FILE = 405L;
	public static final String MSG_PROCESS_FILE = "Error al cargar el archivo ";
	
	public static final Long CODE_OPEN_FILE = 406L;
	public static final String MSG_OPEN_FILE = "Error al cargar el archivo ";
	
	public static final Long CODE_PARSE_NUMBER = 407L;
	public static final String MSG_PARSE_NUMBER = "Error al tratar de convertir el numero";
	
	public static final Long CODE_OUT_OF_BOUNDS = 408L;
	public static final String MSG_OUT_OF_BOUNDS = "Faltan definir valores";
	
	public static final Long CODE_ERROR_DATA= 409L;
	public static final String MSG_ERROR_DATA = "Error en datos: ";
	
	public static final Long CODE_UPLOAD_NULL= 410L;
	public static final String MSG_UPLOAD_NULL = "No se ha definido archivo a cargar";
	
	public static final Long CODE_CSV_FILE= 411L;
	public static final String MSG_CSV_FILE = "El archivo debe ser en formato .csv";
	
	public static final Long CODE_INVALID_PATTERN= 412L;
	public static final String MSG_INVALID_PATTERN = "El archivo debe contener el siguiente patron: PRICELIST_"+PATTERN_DATE_FILE;
	
	public static final Long CODE_INVALID_DATE= 413L;
	public static final String MSG_INVALID_DATE = "El archivo no tiene una fecha valida con el patron: YYYYMMDD";
	
	
	public static final Long CODE_SERVICE_SAVE_UNAVAILABLE= 501L;
	public static final String MSG_SERVICE_SAVE_UNAVAILABLE = "No se pudo acceder al servicio de guardado de nomina";
	
	
}
