package com.beta.htc.load.util;

public class ColumnDefinition {
	
	private Object type;
	private String name;
	private String format;
	public ColumnDefinition(Object type, String name, String format) {
		super();
		this.type = type;
		this.name = name;
		this.format = format;
	}
	public ColumnDefinition() {
		super();
	}
	public Object getType() {
		return type;
	}
	public void setType(Object type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
}
