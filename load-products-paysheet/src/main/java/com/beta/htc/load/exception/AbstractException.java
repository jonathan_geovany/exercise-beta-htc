package com.beta.htc.load.exception;

public class AbstractException extends Exception {
	private static final long serialVersionUID = 1L;
	private static final long DEFAULT_CODE = 400;
	private final long code;
	private final String description;
	private Integer fileLineNumber; 

	public AbstractException(Exception ex) {
		super(ex);
		this.code = DEFAULT_CODE;		
		description = ex.getMessage();
	}

	public AbstractException(long code, String description,Exception ex) {
		super(ex);
		this.code = code;
		this.description = description;
	}

	public AbstractException(long code, String description) {
		super();
		this.code = code;
		this.description = description;
	}

	public long getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}	
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder
		.append("Exception [code=")
		.append(code)
		.append(", description=")
		.append(description)
		.append(", class= ")
		.append(getStackTrace()[0].getClassName())
		.append(", method= ")
		.append(getStackTrace()[0].getMethodName())
		.append(", line ")
		.append(getStackTrace()[0].getLineNumber())
		.append("] ");
		return builder.toString();
	}

	public Integer getFileLineNumber() {
		return fileLineNumber;
	}

	public void setFileLineNumber(Integer fileLineNumber) {
		this.fileLineNumber = fileLineNumber;
	}
}
