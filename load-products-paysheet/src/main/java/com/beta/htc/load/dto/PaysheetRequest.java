package com.beta.htc.load.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PaysheetRequest implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Timestamp created;
	private Integer company;
	
	private List<DetailRequest> details;
	
	public PaysheetRequest() {
		super();
		details = new ArrayList<>();
	}
	
	public PaysheetRequest(Timestamp created) {
		super();
		this.created = created;
		details = new ArrayList<>();
	}
	
	public Date getCreated() {
		return created;
	}
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	public Integer getCompany() {
		return company;
	}
	public void setCompany(Integer company) {
		this.company = company;
	}
	public List<DetailRequest> getDetails() {
		return details;
	}
	public void setDetails(List<DetailRequest> details) {
		this.details = details;
	}
	
}
