package com.beta.htc.load.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class DetailRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idProduct;
	private BigDecimal unitPrice;
	private Integer quantity;
	private Boolean exempt;
	private Boolean cesc;
	private Integer promotion;
	
	public String getIdProduct() {
		return idProduct;
	}
	public void setIdProduct(String idProduct) {
		this.idProduct = idProduct;
	}
	public BigDecimal getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Boolean getExempt() {
		return exempt;
	}
	public void setExempt(Boolean exempt) {
		this.exempt = exempt;
	}
	public Boolean getCesc() {
		return cesc;
	}
	public void setCesc(Boolean cesc) {
		this.cesc = cesc;
	}
	public Integer getPromotion() {
		return promotion;
	}
	public void setPromotion(Integer promotion) {
		this.promotion = promotion;
	}
}
