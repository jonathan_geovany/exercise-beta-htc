package com.beta.htc.operations.config;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {	
	
	@Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)
          .apiInfo(apiInfo())
          //.ignoredParameterTypes(GetMapping.class) //ocultado la entidad
          .select()                                  
          .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
          .paths(PathSelectors.any())       
          .build();                                           
    }
	private ApiInfo apiInfo() {
	    return new ApiInfo(
	      "Operations Products Paysheet", 
	      "Servicio de operaciones CRUD de nomina de costo de productos", 
	      "API SERVICE", 
	      "Terms of service", 
	      new Contact("Jonathan Giovanni", "www.beta-htc.com", "hv12i04001@gmail.com"), 
	      "License of API", "API license URL", Collections.emptyList());
	}
}
