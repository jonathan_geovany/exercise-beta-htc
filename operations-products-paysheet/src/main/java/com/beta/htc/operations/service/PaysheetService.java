package com.beta.htc.operations.service;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;

import com.beta.htc.operations.dto.Container;
import com.beta.htc.operations.dto.PaysheetRequest;
import com.beta.htc.operations.entity.Paysheet;

public interface PaysheetService {
	public Container savePaysheet(PaysheetRequest request);
	public List<Container> saveMultiplePaysheets(List<PaysheetRequest> requests);
	public ResponseEntity<Object> invalidatePaysheet(int id);
	public Optional<Paysheet> findById(int id);
}
