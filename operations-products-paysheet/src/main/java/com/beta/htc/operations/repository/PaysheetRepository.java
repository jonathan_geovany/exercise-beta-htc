package com.beta.htc.operations.repository;

import java.sql.Timestamp;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.beta.htc.operations.entity.Paysheet;

public interface PaysheetRepository extends JpaRepository<Paysheet, Integer>{
	public Optional<Paysheet> findByCreated(Timestamp time);
	
	@Query(nativeQuery=true,value="SELECT CASE WHEN COUNT(p)<1 THEN true ELSE false END from paysheet p WHERE p.created>?1")
	public Boolean isTheLastPaysheet(Timestamp inserted);
}
