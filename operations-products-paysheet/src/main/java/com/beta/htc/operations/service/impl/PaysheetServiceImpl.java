package com.beta.htc.operations.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.beta.htc.operations.dto.Container;
import com.beta.htc.operations.dto.PaysheetDetailRequest;
import com.beta.htc.operations.dto.PaysheetRequest;
import com.beta.htc.operations.entity.Company;
import com.beta.htc.operations.entity.Paysheet;
import com.beta.htc.operations.entity.PaysheetDetail;
import com.beta.htc.operations.entity.PaysheetTotal;
import com.beta.htc.operations.entity.PriceMovement;
import com.beta.htc.operations.entity.PriceRange;
import com.beta.htc.operations.entity.Product;
import com.beta.htc.operations.entity.ProductPrice;
import com.beta.htc.operations.entity.TaxMovement;
import com.beta.htc.operations.repository.CompanyRepository;
import com.beta.htc.operations.repository.PaysheetRepository;
import com.beta.htc.operations.repository.PriceRangeRepository;
import com.beta.htc.operations.repository.ProductRepository;
import com.beta.htc.operations.repository.TaxRepository;
import com.beta.htc.operations.service.PaysheetService;
import com.beta.htc.operations.util.AppConst;
import com.beta.htc.operations.util.OpError;


@Service
public class PaysheetServiceImpl implements PaysheetService {

	private static final String CODE = "Code";
	private static final String DESCRIPTION = "Description";

	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private PaysheetRepository paysheetRepository;
	@Autowired
	private CompanyRepository companyRepository;
	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private PriceRangeRepository priceRangeRepository;
	@Autowired
	private TaxRepository taxRepository;
	
	@Override
	public Container savePaysheet(PaysheetRequest request) {
		//contenedor de los errores
		Container container 		= new Container();
		try {
			//entidad de nomina de productos
			Paysheet paysheet			= new Paysheet();
			//lista de detalles de nomina de productos
			List<PaysheetDetail> details= new ArrayList<>();
			//buscar company dado el id proporcionado
			Optional<Company> companySearched = companyRepository.findById(request.getCompany());
			Company company				= null;
			
			//obtener valores de impuestos
			BigDecimal iva 	= taxRepository.findByDescription("IVA").getPercentage();
			BigDecimal cesc = taxRepository.findByDescription("CESC").getPercentage();
			
			//si no se encontro la company se agrega un error
			if(!companySearched.isPresent()) {
				container.addError(new OpError(AppConst.CODE_NOT_FOUND_COMPANY, 0, AppConst.MSG_NOT_FOUND_COMPANY+request.getCompany()));
				log.error(AppConst.MSG_NOT_FOUND_COMPANY+request.getCompany());	
			}else company = companySearched.get();//si se encontro se guarda la company
			
			//verificar si es la ultima nomina insertada
			boolean lastPaysheet = paysheetRepository.isTheLastPaysheet(request.getCreated());
			log.info("Es ultima nomina ? {}",lastPaysheet);	
			//si ya hay una nomina bajo la misma fecha se agrega error de dato duplicado
			if(paysheetRepository.findByCreated(request.getCreated()).isPresent()) {
				StringBuilder builder = new StringBuilder();
				builder.append(AppConst.MSG_DUPLIED_PAYSHEET);
				builder.append(request.getCreated().toString());
				container.addError(new OpError(AppConst.CODE_DUPLIED_PAYSHEET, 0,builder.toString()));
				log.error(builder.toString());	
			}
			//se establecen valores de cabecera a la nomina
			paysheet.setCreated(request.getCreated());
			paysheet.setCompany(company);
			paysheet.setAnnulled(false);
			
			int pos=1;//posicion de iteracion en la lista de detalles
			
			//inicializando los totales con cero
			List<PaysheetTotal> totals = new ArrayList<>();
			for (PriceRange range : priceRangeRepository.findAllByEnable(true)) {
				PaysheetTotal total = new PaysheetTotal();
				total.setTotalPrice(new BigDecimal(0));
				total.setPriceRange(range);
				totals.add(total);
			}
			//guardar lista de productos repetidos
			List<String> repeatedCodes = new ArrayList<>();
			
			//recorrer cada elemento detalle insertado
			for (PaysheetDetailRequest detailRequest : request.getDetails()) {
				//buscar el producto dado en codigo insertado
				Product product = productRepository.findByCode(detailRequest.getIdProduct());
				//si el producto es diferente de nulo entonces se procede a realizar los calculos
				if(product!=null) {
					//si el producto no esta repetido
					Long repeated = request.getDetails().stream().filter(d->d.getIdProduct().equals(detailRequest.getIdProduct())).count();
					if(repeated<=1) {
						//creando detalle de nomina y agregando valores de cabecera
						PaysheetDetail detail = new PaysheetDetail();
						detail.setCesc(detailRequest.getCesc());
						detail.setDiscount( BigDecimal.valueOf( detailRequest.getPromotion()/100.0 ) );
						detail.setExempt(detailRequest.getExempt());
						detail.setQuantity(detailRequest.getQuantity());
						detail.setUnitPrice(detailRequest.getUnitPrice());
						//agregando productos al stock
						product.setStock( product.getStock()+ detail.getQuantity() );
						//estableciendo el descuento al producto si es la ultima nomina
						if(lastPaysheet) product.setDiscount(detail.getDiscount());
						//estableciendo nuevo precio unitario al producto si es la ultima nomina
						if(lastPaysheet) product.setUnitPrice(detail.getUnitPrice());
						//creando movimiento de impuestos - respalda inf de impuestos y calculos
						TaxMovement taxMovement = new TaxMovement();
						//por defecto se establece el IVA
						BigDecimal prodIva		= iva;
						//por defecto se establece cero al CESC
						BigDecimal prodCesc		= new BigDecimal(0);
						//si esta exento se establece IVA a cero
						if(detail.getExempt()) 	prodIva	= new BigDecimal(0);
						//si tiene CESC se establece el valor del CESC
						if(detail.getCesc())	prodCesc= cesc;
						//descuento = precio_unitario*valor_descuento
						taxMovement.setDiscount(detail.getUnitPrice().multiply(detail.getDiscount()));
						//respaldo de porcentajes de CESC e IVA
						taxMovement.setCesc(prodCesc);
						taxMovement.setIva(prodIva);
						//subtotal_impuesto = precio_unitario*(IVA+CESC)
						taxMovement.setSubtotal( detail.getUnitPrice().multiply(prodCesc.add(prodIva)));
						//asociando el movimiento de impuesto al detalle
						detail.setTaxMovement(taxMovement);
						//recorrer cada uno de los rangos de precios que esta habilitados y guardarlos en una lista
						List<PriceMovement> pricesMovements 	= new ArrayList<>();
						//creando iteradores para recorrer los rangos de precios y los precios de producto
						Iterator<PriceRange> itePriceRange		= priceRangeRepository.findAllByEnable(true).iterator();
						Iterator<ProductPrice> iteProductPrice	= product.getPrices().iterator();
						Iterator<PaysheetTotal> itTotal			= totals.iterator();
						while (itePriceRange.hasNext() && iteProductPrice.hasNext()) {
							//creando movimiento de precio para cada rango
							PriceMovement priceMovement = new PriceMovement();
							//obteniendo objetos del iterador
							PriceRange range 	= itePriceRange.next();
							ProductPrice productPrice		= iteProductPrice.next();
							PaysheetTotal acumulativeTotal 	= itTotal.next();
							//estableciendo el porcentage
							priceMovement.setPercentage(range.getPercentage());
							//total = (precio unitario - descuento) + subtotal_impuestos + (precio unitario*porcentaje)
							BigDecimal total = (detail.getUnitPrice().subtract(taxMovement.getDiscount())).add(taxMovement.getSubtotal()).add( detail.getUnitPrice().multiply(range.getPercentage()));
							priceMovement.setTotalPrice(total);
							//si es la ultima nomina entonces se actualizan los precios
							if(lastPaysheet) productPrice.setTotalPrice(total);
							//acumulando los utilidades = (total - precio)*cantidad en cada rango de precio
							acumulativeTotal.setTotalPrice(acumulativeTotal.getTotalPrice().add( total.subtract(detail.getUnitPrice()).multiply( BigDecimal.valueOf(detail.getQuantity())) ));
							//asociando el rango al precio
							priceMovement.setPriceRange(range);
							//agregando a la lista de precios
							pricesMovements.add(priceMovement);
						}
						//agregando el producto al detalle
						detail.setProduct(product);
						//agregando la lista de precios validos
						detail.setPrices(pricesMovements);
						//agregando el detalle a la lista de detalles
						details.add(detail);
					}else {
						//si no esta repetido, en mi lista de codigos repetidos agrego el error
						if(repeatedCodes.stream().noneMatch(code->code.equals(detailRequest.getIdProduct()))) {
							repeatedCodes.add(detailRequest.getIdProduct());
							StringBuilder builder = new StringBuilder();
							builder.append(AppConst.MSG_REPEATED_PRODUCT);
							builder.append(detailRequest.getIdProduct());
							builder.append(" cantidad "+(repeated-1));
							container.addError(new OpError(AppConst.CODE_REPEATED_PRODUCT, pos,builder.toString()));
							log.error(builder.toString());
						}
					}
				}else {
					//si no se encontro el producto se agrega a la lista de errores
					StringBuilder builder = new StringBuilder();
					builder.append(AppConst.MSG_NOT_FOUND_PRODUCT);
					builder.append(detailRequest.getIdProduct());
					container.addError(new OpError(AppConst.CODE_NOT_FOUND_PRODUCT, pos,builder.toString()));
					log.error(builder.toString());	
				}
				pos++;//incrementando la posicion en la lista de detalle esto para saber que linea en el registro tiene error
			}
			//agregando la lista de detalles a la nomina
			paysheet.setDetails(details);
			//agregando la lista de totales a la nomina
			paysheet.setTotals(totals);
			//si no tiene errores entonces se guarda la nomina de costeo de productos
			if(container.getErrors().isEmpty()) {
				paysheet = paysheetRepository.saveAndFlush(paysheet);
				log.info("insertado correctamente id "+paysheet.getId());
			}else {
				log.info("nomina contiene errores");
				paysheet=null;
				container.setCode(AppConst.CODE_BAD_REQUEST);
				container.setDescription(AppConst.MSG_BAD_REQUEST);
			}
			//guardando la nomina en el contenedor
			container.setPaysheet(paysheet);
			
		}catch (Exception e) {
			log.error(e.getMessage());
			container.addError(new OpError(AppConst.CODE_SERVER_SAVE, 0,AppConst.MSG_SERVER_SAVE));
			container.setPaysheet(null);
			container.setCode(AppConst.CODE_SERVER_SAVE);
			container.setDescription(AppConst.MSG_SERVER_SAVE);
		}
		return container;
	}

	@Override
	public List<Container> saveMultiplePaysheets(List<PaysheetRequest> requests) {
		return requests.stream().map( r-> savePaysheet(r)).collect(Collectors.toList());
	}

	@Override
	public ResponseEntity<Object> invalidatePaysheet(int id) {
		Optional<Paysheet> paysheetSearched = paysheetRepository.findById(id);
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status = HttpStatus.BAD_REQUEST;
		if(paysheetSearched.isPresent()) {
			Paysheet paysheet = paysheetSearched.get();
			//si ya esta anulada
			if(paysheet.getAnnulled()) {
				headers.add(DESCRIPTION, AppConst.MSG_ALREADY_ANNULLED+id);
				headers.add(CODE, String.valueOf(AppConst.CODE_ALREADY_ANNULLED) );
			}else {// si no esta anulada entonces la anulo- recorrer cada producto asociado y restarle el stock
				paysheet.getDetails().forEach(d-> d.getProduct().setStock( d.getProduct().getStock() - d.getQuantity()));
				paysheet.setAnnulled(true);
				paysheetRepository.saveAndFlush(paysheet);
				status = HttpStatus.OK;
				headers.add(DESCRIPTION, AppConst.MSG_OK);
				headers.add(CODE, String.valueOf(AppConst.CODE_OK) );
			}
		}else {
			headers.add(DESCRIPTION, AppConst.MSG_NOT_FOUND_PAYSHEET+id);
			headers.add(CODE, String.valueOf(AppConst.CODE_NOT_FOUND_PAYSHEET) );
		}
		return new ResponseEntity<>(null,headers,status);
	}

	@Override
	public Optional<Paysheet> findById(int id) {
		return paysheetRepository.findById(id);
	}

}
