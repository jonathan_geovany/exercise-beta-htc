package com.beta.htc.operations.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.beta.htc.operations.entity.PaysheetTotal;

public interface PaysheetTotalRepository extends JpaRepository<PaysheetTotal, Integer> {

}
