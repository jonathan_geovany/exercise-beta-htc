package com.beta.htc.operations.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.beta.htc.operations.entity.PaysheetDetail;

public interface PaysheetDetailRepository extends JpaRepository<PaysheetDetail, Integer>{

}
