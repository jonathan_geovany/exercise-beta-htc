package com.beta.htc.operations.handler;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.beta.htc.operations.dto.Container;
import com.beta.htc.operations.util.AppConst;
import com.beta.htc.operations.util.OpError;


@ControllerAdvice
public class ResponseHttpExceptionHandler extends ResponseEntityExceptionHandler {
	
	private Logger log = LoggerFactory.getLogger(getClass());
	
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
		log.error(ex.getMessage());
		Container container = new Container();
		container.addError(new OpError(AppConst.CODE_SERVER_SAVE, 0,AppConst.MSG_SERVER_SAVE));
		container.setPaysheet(null);
		container.setCode(AppConst.CODE_SERVER_SAVE);
		container.setDescription(AppConst.MSG_SERVER_SAVE);
		return new ResponseEntity<>(container,HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
		HttpHeaders headers, HttpStatus status, WebRequest request) {

		log.error(ex.getMessage());
		
		Container container = new Container();
		
		for (Object error : ex.getBindingResult().getAllErrors()) {
			if(error instanceof FieldError) {
		        FieldError fieldError = (FieldError) error;
		        String field = fieldError.getField();
		        if(field.contains(".")) field = field.split("\\.")[1];
		        StringBuilder builder = new StringBuilder();
		        builder.append(field);
		        builder.append(" ");
		        builder.append(fieldError.getDefaultMessage());
		        container.addError(new OpError(AppConst.CODE_BAD_REQUEST, 0,  builder.toString()));
			}
		}
		container.setCode(AppConst.CODE_BAD_REQUEST);
		container.setDescription(AppConst.MSG_BAD_REQUEST);
		return new ResponseEntity<>(container, HttpStatus.BAD_REQUEST);
	}	
	
}
