package com.beta.htc.operations.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "product")
public class Product implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -8730388229579632107L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "code")
	private String code;
	@Column(name = "name")
	private String name;
	@Column(name = "stock")
	private Integer stock;
	@Column(name = "unit_price")
	private BigDecimal unitPrice;
	@Column(name = "discount")
	private BigDecimal discount; 
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy="product")
	@JsonIgnore
	private List<ProductPrice> prices;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy="product")
	@JsonIgnore
    private List<PaysheetDetail> details;
	
	
	public List<ProductPrice> getPrices() {
		return prices;
	}
	public void setPrices(List<ProductPrice> prices) {
		this.prices = prices;
		this.prices.forEach(p->p.setProduct(this));
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getStock() {
		return stock;
	}
	public void setStock(Integer stock) {
		this.stock = stock;
	}
	public BigDecimal getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Product [id=");
		builder.append(id);
		builder.append(", code=");
		builder.append(code);
		builder.append(", name=");
		builder.append(name);
		builder.append(", stock=");
		builder.append(stock);
		builder.append(", unitPrice=");
		builder.append(unitPrice);
		builder.append(", discount=");
		builder.append(discount);
		
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
