package com.beta.htc.operations.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "paysheet")
public class Paysheet implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4279275738143345283L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "created")
	private Timestamp created;
	@ManyToOne
	@JoinColumn(name = "id_company")
	private Company company;
	@Column(name="annulled")
	private Boolean annulled;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy="paysheet")
	private List<PaysheetDetail> details;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy="paysheet")
	private List<PaysheetTotal> totals;
	
	
	
	
	public List<PaysheetTotal> getTotals() {
		return totals;
	}
	public void setTotals(List<PaysheetTotal> totals) {
		this.totals = totals;
		this.totals.forEach(t->t.setPaysheet(this));
	}
	public List<PaysheetDetail> getDetails() {
		return details;
	}
	public void setDetails(List<PaysheetDetail> details) {
		this.details = details;
		this.details.forEach(d-> d.setPaysheet(this));
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Timestamp getCreated() {
		return created;
	}
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public Boolean getAnnulled() {
		return annulled;
	}
	public void setAnnulled(Boolean annulled) {
		this.annulled = annulled;
	}
}
