package com.beta.htc.operations.dto;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import com.beta.htc.operations.entity.Paysheet;
import com.beta.htc.operations.util.AppConst;
import com.beta.htc.operations.util.OpError;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class Container {

	//TODO
	private URI uri;
	private String fileName="prueba";
	
	@JsonIgnore
	private Paysheet paysheet;
	@JsonIgnore
	private Boolean paysheetOk=true;
	
	private List<OpError> errors;
	private Long code = AppConst.CODE_OK;
	private String description = AppConst.MSG_OK;
	
	public Container() {
		super();
		this.errors = new ArrayList<>();
	}
	public Container(Paysheet paysheet, List<OpError> errors) {
		super();
		this.paysheet = paysheet;
		this.errors = errors;
		if(errors!=null) paysheetOk=false;
	}
	public Container(OpError error) {
		this.errors = new ArrayList<>();
		this.errors.add(error);
		if(error!=null) paysheetOk=false;
	}
	
	
	
	public Paysheet getPaysheet() {
		return paysheet;
	}
	public void setPaysheet(Paysheet paysheet) {
		this.paysheet = paysheet;
	}
	public List<OpError> getErrors() {
		return errors;
	}
	public void setErrors(List<OpError> errors) {
		this.errors = errors;
	}
	
	public void addError(OpError error) {
		if(this.errors==null) this.errors =  new ArrayList<>();
		if(error!=null) {
			this.errors.add(error);
			this.paysheetOk=false;
		}
	}
	
	public Boolean getPaysheetOk() {
		if(!paysheetOk) return paysheetOk;
		this.paysheetOk = this.errors.isEmpty();
		return this.paysheetOk;
	}
	public Long getCode() {
		return code;
	}
	public void setCode(Long code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public URI getUri() {
		return uri;
	}
	public void setUri(URI uri) {
		this.uri = uri;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	
}
