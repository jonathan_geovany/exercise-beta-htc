package com.beta.htc.operations.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.beta.htc.operations.dto.Container;
import com.beta.htc.operations.dto.PaysheetRequest;
import com.beta.htc.operations.service.PaysheetService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@RestController
public class OperationsController {

	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private PaysheetService paysheetService;
	
	@HystrixCommand // da problemas con ServletUriComponentsBuilder.fromCurrentContextPath()... no encuentra el contexto
	@PostMapping(value="/save")
	Container savePaysheet(@Valid @RequestBody PaysheetRequest request) {
		Container container = paysheetService.savePaysheet(request);
		if(container.getErrors().isEmpty()) {
			try {
				container.setUri(ServletUriComponentsBuilder.fromUriString("http://localhost:8765/query-products-paysheet/paysheet/id/{id}/").buildAndExpand(container.getPaysheet().getId()).toUri());		
			} catch (Exception e) {
				log.error(e.getMessage());
				container.setUri(URI.create("http://localhost:8765/query-products-paysheet/paysheet/id/"+container.getPaysheet().getId()+"/"));
			}
		}
		return container;
	}
	
	@HystrixCommand
	@GetMapping(value="/invalidate/{id}")
	ResponseEntity<Object> invalidatePaysheet(@PathVariable int id) {
		return paysheetService.invalidatePaysheet(id);
	}
	
	@HystrixCommand
	@PostMapping(value="/save-multiple")
	List<Container> saveMultiplePaysheets(@Valid @RequestBody List<PaysheetRequest> requests) {
		List<Container> containers = paysheetService.saveMultiplePaysheets(requests);
		containers.forEach( c-> {
			if(c.getErrors().isEmpty()) {
				try {
					c.setUri(ServletUriComponentsBuilder.fromUriString("http://localhost:8765/query-products-paysheet/paysheet/id/{id}/").buildAndExpand(c.getPaysheet().getId()).toUri());		
				} catch (Exception e) {
					log.error(e.getMessage());
					c.setUri(URI.create("http://localhost:8765/query-products-paysheet/paysheet/id/"+c.getPaysheet().getId()+"/"));
				}
			}
		} );
		return containers;
	}
	
	@HystrixCommand
	@PostMapping(value="/test")
	public String test(@RequestBody String msg) {
		return msg+" operations service ";
	}
}