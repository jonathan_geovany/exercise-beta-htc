package com.beta.htc.operations.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.beta.htc.operations.entity.TaxMovement;

public interface TaxMovementRepository extends JpaRepository<TaxMovement, Integer> {

}
