package com.beta.htc.operations.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "tax_movement")
public class TaxMovement implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8959483558489990138L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@OneToOne
    @JoinColumn(name = "id_paysheet_detail", referencedColumnName = "id")
	@JsonIgnore
	private PaysheetDetail paysheetDetail;
	@Column(name = "iva")
	private BigDecimal iva;
	@Column(name = "discount")
	private BigDecimal discount;
	@Column(name = "cesc")
	private BigDecimal cesc;
	@Column(name = "subtotal")
	private BigDecimal subtotal;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public PaysheetDetail getPaysheetDetail() {
		return paysheetDetail;
	}
	public void setPaysheetDetail(PaysheetDetail paysheetDetail) {
		this.paysheetDetail = paysheetDetail;
	}
	public BigDecimal getIva() {
		return iva;
	}
	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public BigDecimal getCesc() {
		return cesc;
	}
	public void setCesc(BigDecimal cesc) {
		this.cesc = cesc;
	}
	public BigDecimal getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}
	
	
}
