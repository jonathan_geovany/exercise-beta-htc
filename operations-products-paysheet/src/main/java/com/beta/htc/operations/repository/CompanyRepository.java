package com.beta.htc.operations.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.beta.htc.operations.entity.Company;

public interface CompanyRepository extends JpaRepository<Company, Integer>{
}
