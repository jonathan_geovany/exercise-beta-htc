package com.beta.htc.operations.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.beta.htc.operations.entity.ProductPrice;

public interface ProductPriceRepository extends JpaRepository<ProductPrice, Integer> {

}
