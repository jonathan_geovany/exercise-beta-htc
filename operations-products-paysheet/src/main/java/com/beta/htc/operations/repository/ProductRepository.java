package com.beta.htc.operations.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.beta.htc.operations.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {
	public Product findByCode(String code);
}
