package com.beta.htc.operations.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.beta.htc.operations.entity.PriceMovement;

public interface PriceMovementRepository extends JpaRepository<PriceMovement, Integer> {

}
