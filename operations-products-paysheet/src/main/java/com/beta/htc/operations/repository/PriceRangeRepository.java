package com.beta.htc.operations.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.beta.htc.operations.entity.PriceRange;

public interface PriceRangeRepository extends JpaRepository<PriceRange, Integer> {
	public List<PriceRange> findAllByEnable(Boolean enable);
}
