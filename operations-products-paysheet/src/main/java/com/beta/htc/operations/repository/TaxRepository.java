package com.beta.htc.operations.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.beta.htc.operations.entity.Tax;

public interface TaxRepository extends JpaRepository<Tax, Integer>{
	public Tax findByDescription(String description);
}
