package com.beta.htc.operations.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class PaysheetRequest implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//@JsonFormat(pattern="yyyy-MM-dd HH:mm")
	@NotNull
	private Timestamp created;
	@NotNull
	private Integer company;
	@NotEmpty
	@Valid
	private List<PaysheetDetailRequest> details;
	
	public Timestamp getCreated() {
		return created;
	}
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	public Integer getCompany() {
		return company;
	}
	public void setCompany(Integer company) {
		this.company = company;
	}
	public List<PaysheetDetailRequest> getDetails() {
		return details;
	}
	public void setDetails(List<PaysheetDetailRequest> details) {
		this.details = details;
	}
}
