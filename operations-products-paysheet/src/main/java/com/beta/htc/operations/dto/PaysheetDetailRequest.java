package com.beta.htc.operations.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class PaysheetDetailRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@NotNull
	private String idProduct;
	@NotNull
	@Positive
	private BigDecimal unitPrice;
	@NotNull
	@Positive
	private Integer quantity;
	@NotNull
	private Boolean exempt;
	@NotNull
	private Boolean cesc;
	@NotNull
	private Integer promotion;
	
	public String getIdProduct() {
		return idProduct;
	}
	public void setIdProduct(String idProduct) {
		this.idProduct = idProduct;
	}
	public BigDecimal getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Boolean getExempt() {
		return exempt;
	}
	public void setExempt(Boolean exempt) {
		this.exempt = exempt;
	}
	public Boolean getCesc() {
		return cesc;
	}
	public void setCesc(Boolean cesc) {
		this.cesc = cesc;
	}
	public Integer getPromotion() {
		return promotion;
	}
	public void setPromotion(Integer promotion) {
		this.promotion = promotion;
	}
}
