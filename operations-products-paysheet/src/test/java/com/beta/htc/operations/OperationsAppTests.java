package com.beta.htc.operations;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.beta.htc.operations.entity.Company;
import com.beta.htc.operations.entity.PriceRange;
import com.beta.htc.operations.entity.Product;
import com.beta.htc.operations.entity.ProductPrice;
import com.beta.htc.operations.entity.Tax;
import com.beta.htc.operations.repository.CompanyRepository;
import com.beta.htc.operations.repository.PriceRangeRepository;
import com.beta.htc.operations.repository.ProductPriceRepository;
import com.beta.htc.operations.repository.ProductRepository;
import com.beta.htc.operations.repository.TaxRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OperationsAppTests {
	Logger log = LoggerFactory.getLogger(getClass());
	
	
	//tablas maestras
	@Autowired
	private CompanyRepository companyRepository;
	@Autowired
	private PriceRangeRepository priceRangeRepository;
	@Autowired
	private TaxRepository taxRepository;
	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private ProductPriceRepository productPriceRepository;
	
	
	@Test
	public void contextLoads() {
		
		for (Company c : companyRepository.findAll()) {
			log.info(c.toString());
		}
		
		for (Tax t : taxRepository.findAll()) {
			log.info(t.toString());
		}
		for (PriceRange p : priceRangeRepository.findAll()) {
			log.info(p.toString());
		}
		for (Product p : productRepository.findAll()) {
			log.info(p.toString());
		}
		
		/*List<ProductPrice> productPrices= new ArrayList<>();
		//insertar precios de productos en product price
		for (Product product : productRepository.findAll()) {
			for (PriceRange range : priceRangeRepository.findAll()) {
				ProductPrice productPrice = new ProductPrice();
				productPrice.setProduct(product);
				productPrice.setPriceRange(range);
				productPrice.setTotalPrice( product.getUnitPrice().multiply( range.getPercentage() ) );
				productPrices.add(productPrice);
			}
		}
		
		productPriceRepository.saveAll(productPrices);
		*/
		for (ProductPrice productPrice : productPriceRepository.findAll()) {
			log.info(productPrice.toString());
		}
		
		
	}

}

