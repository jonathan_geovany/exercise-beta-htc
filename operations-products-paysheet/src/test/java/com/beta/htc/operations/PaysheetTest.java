package com.beta.htc.operations;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.beta.htc.operations.entity.Product;
import com.beta.htc.operations.entity.ProductPrice;
import com.beta.htc.operations.repository.ProductRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PaysheetTest {
	Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private ProductRepository productRepository;
	
	//@Transactional
	@Test
	public void paysheetTest() {
		for (Product p : productRepository.findAll()) {
			for (ProductPrice pr : p.getPrices()) {
				log.info("producto {}",pr.toString());
			}
		}
	}
}
