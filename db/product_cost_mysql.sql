create database product_cost_db;
use product_cost_db; 


-- tablas maestras

create table company(
	id 				integer 		primary key auto_increment,
	tradename		text			not null,
	business_name	text			not null,
	sector			varchar(60)		,
	nit				varchar(20)		not null,
	nrc				varchar(20)		not null,
	address			text			not null,
	phone			varchar(20)		,
	email			varchar(60)		,
	tax				numeric(4,2)	not null 	-- iva debe ser en constante, o esta bien que vaya en la empresa?
);

create table category(
	id				integer 		primary key auto_increment,
	description		text			not null,
	exempt			boolean			not null,	-- exentos: alimentos, libros, ...?
	cesc			numeric(4,2)	not null	-- impuesto contribucion especial: 0 si no tiene, 0.05 es para telefonia, internet y tv 
);

-- cuatro rangos de precios: 1-empleado 15% , 2-mayorista 25%, 3-centro escolar 35% , 4- sala de venta 45%
create table price_range(
	id				integer 		primary key auto_increment,
	description		text 			not null,
	percentage		numeric(4,2) 	not null,
    enable			boolean			not null
);

-- tablas maestro-detalle

create table product(
	id				integer 		primary key auto_increment,
	id_category		integer			not null,
	code			varchar(80)		unique not null,
	name			text			not null,
	stock			integer 		not null,
	price			numeric(10,4)	not null, 
	discount		numeric(10,4)	not null,
    foreign key (id_category) references category(id) 
);


create table paysheet(
	id				integer 		primary key auto_increment,
	created			timestamp		not null,
	id_company		integer			not null,
    foreign key (id_company) references company(id)
);

create table paysheet_detail(
	id				integer 		primary key auto_increment,
    id_paysheet		integer 		not null unique, -- solamente un producto por cada registro en la nomina
	id_product		integer 		not null unique, -- solamente un producto por cada registro en la nomina
	unit_price		numeric(10,4)	not null, -- precio del producto se almacena por si cambia en el futuro
	quantity		integer			not null, -- iva de la empresa se almacena por si cambia en el futuro
	total_iva		numeric(10,4) 	not null, -- precio * iva
	total_cesc		numeric(10,4)	not null, -- precio * (precio * cesc) duda si esto solo debe ser calculado y no guardado en un campo precio * cesc , debe ser el precio base del producto o el precio obtenido con iva?
	total_discount	numeric(10,4)	not null, -- precio * descuento, debe ser el precio base del producto o el precio obtenido con iva?
	subtotal		numeric(10,4)	not null, -- en base a los calculos de iva, cesc, y descuento
	foreign key (id_paysheet) references paysheet(id),
    foreign key (id_product) references product(id)
);


create table total_price(
	id				integer 		primary key auto_increment,
    id_price_range		integer 	not null, -- solamente un producto por cada registro en la nomina
	id_paysheet_detail	integer 	not null, -- solamente un producto por cada registro en la nomina
	total				numeric(10,4) not null,
    foreign key (id_price_range) references price_range(id),
    foreign key (id_paysheet_detail) references paysheet_detail(id)
);

create table movements(
	id				serial 			primary key,
	id_product			integer     not null, 
	id_paysheet_detail	integer 	not null, 
	unit_price		numeric(10,4)	not null, -- precio del producto afecta el precio del stock
	iva				numeric(4,2)	not null, -- mover a tabla de movimientos
	discount		numeric(4,2)	not null,
    foreign key (id_product) references product(id),
    foreign key (id_paysheet_detail) references paysheet_detail(id)
);
