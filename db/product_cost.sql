
-- tablas maestras
create table tax(
	id				serial 			primary key,
	description		varchar(80) 	unique not null,
	percentage		numeric(6,4) 	not null
);

create table company(
	id 				serial 			primary key,
	name			text			not null,
	sector			varchar(60)		,
	nit				varchar(20)		not null,
	address			text			not null,
	phone			varchar(20)			
);

-- cuatro rangos de precios: 1-empleado 15% , 2-mayorista 25%, 3-centro escolar 35% , 4- sala de venta 45%
create table price_range(
	id				serial 			primary key,
	description		text 			not null,
	percentage		numeric(6,4) 	not null,
	enable			boolean			not null -- si esta activado entonces realizo los totales
);

create table product(
	id				serial 			primary key,
	code			varchar(80)		unique not null,
	name			text			not null,
	stock			integer 		not null,
	unit_price		numeric(20,4)	not null, -- actualizar el precio unitario cuando se agregue una nomina con ese producto
	discount		numeric(20,4)	not null  -- actualizar descuento
);

create table product_price(
	id				serial 			primary key,
	id_product		integer			references product(id) not null,
	id_price_range	integer			references price_range(id) not null,
	total_price		numeric(20,4)	not null
);

create table paysheet(
	id				serial 			primary key,
	created			timestamp		unique not null,
	id_company		integer			references company(id) not null,
	annulled		boolean 		not null
);

create table paysheet_detail(
	id				serial 			primary key,
	id_paysheet		integer 		references paysheet(id) not null, -- solamente un producto por cada registro en la nomina
	id_product		integer 		references product(id) not null, -- solamente un producto por cada registro en la nomina
	unit_price		numeric(20,4)	not null, -- precio del producto afecta el precio del stock
	quantity		integer			not null, -- debe sumar al stock
	exempt			boolean			not null, -- exentos: alimentos, libros, ...?
	cesc			boolean			not null, -- impuesto contribucion especial
	discount		numeric(20,4)	not null -- debe afectar al descuento del producto?
);

create table paysheet_total(
	id				serial 			primary key,
	id_paysheet		integer			references paysheet(id) not null,
	id_price_range	integer			references price_range(id) not null,
	total_price		numeric(20,4)	not null
);

create table tax_movement(
	id					serial 		primary key,
	id_paysheet_detail	integer 	references paysheet_detail(id) not null, 
	-- solo guardar porcentajes no calculos
	iva				numeric(20,4)	not null, -- mover a tabla de movimientos
	discount		numeric(20,4)	not null,
	cesc			numeric(20,4)	not null, -- impuesto contribucion especial
	-- guardar totales
	subtotal		numeric(20,4)	not null -- en base a los calculos de iva, cesc, y descuento
);

create table price_movement(
	id					serial 		primary key,
	id_price_range		integer 	references price_range(id) not null, -- solamente un producto por cada registro en la nomina
	id_paysheet_detail	integer 	references paysheet_detail(id) not null, -- solamente un producto por cada registro en la nomina
	percentage			numeric(6,4) 	not null,
	total_price			numeric(20,4)	not null 
);

-- datos de prueba

insert into company(name,sector,nit,address,phone) values 
	('Empresa Ejemplo S.A. de C.V.','ventas','0614-290290-001-0','San Salvador Col. Escalon 2a Av Norte #2-4','(+503) 2429 0505');




insert into tax(description,percentage) values
	('IVA',0.13),
	('CESC',0.05);


insert into price_range(description,percentage,enable) values
	('empleado',0.15,true),
	('mayorista',0.25,true),
	('centro escolar',0.35,true),
	('sala de venta',0.45,true);

insert into product(code,name,stock,unit_price,discount) values
	('04B01001','BOLIGRAFO BIC MEDIANO',1000,0.04,0), 
	('04B01002','BOLIGRAFO LINE BALL PTO 0.50',2000,0.05,0),
	('02F02003','FOLDER MANILA T/C ALAS DORADAS X100',5000,0.035,0),
	('612668140099','RESMA PAPEL BOND T/C X500',25,3.75,0), -- CODIGO MODIFICADO POR QUE EL DEL CSV NO ESTA CORRECTO
	('121548798795','CUADERNO CONQUISTADOR #3 RAYADO',500,0.63,0), -- CODIGO MODIFICADO POR QUE EL DEL CSV NO ESTA CORRECTO
	('05C01002','CUADERNO BEXCELLENT #1 C/GRANDE',250,0.7,0),
	('121377689991','CUADERNO SCRIBE SUPER HEROES #3 LISO',100,1.15,0.08),-- CODIGO MODIFICADO POR QUE EL DEL CSV NO 
	('10P01001','PAPEL DE REGALO',500,0.25,0),
	('13L01001','LIBRO SEMBRADOR #3',20,2.80,0),
	('13L01002','LIBRO DE LEYES TRIBUTARIAS 2018',5,15.00,0),
	('13L01003','CONSTITUCIÓN DE LA REPÚBLICA EL SALVADOR',3,20.00,0),
	('13L01004','LIBRO SEMBRADOR #6',20,2.50,0),
	('15L01001','MEMORIAS USB KINGSTON 32GB',10,20.00,0),
	('15C01001','SMARTPHONE SAMSUNG S8',5,400.00,0.10),
	('15C01002','SMARTPHONE SAMSUNG C5',3,200.00,0);
	
insert into product_price(id_product,id_price_range,total_price) values 
	(1,1,0.006), -- 0.15
	(1,2,0.01),  -- 0.25
	(1,3,0.014), -- 0.35
	(1,4,0.018), -- 0.45
	(2,1,0.006), -- 0.15
	(2,2,0.01),  -- 0.25
	(2,3,0.014), -- 0.35
	(2,4,0.018), -- 0.45
	(3,1,0.006), -- 0.15
	(3,2,0.01),  -- 0.25
	(3,3,0.014), -- 0.35
	(3,4,0.018), -- 0.45
	(4,1,0.006), -- 0.15
	(4,2,0.01),  -- 0.25
	(4,3,0.014), -- 0.35
	(4,4,0.018), -- 0.45
	
	
	

-- consultas de prueba

select * from product p INNER JOIN product_price p_p on p.id = p_p.id_product ORDER BY p.id;


select * from product p INNER JOIN paysheet_detail d on p.id = d.id_product ORDER BY p.id;

select * from paysheet_detail;


select * from paysheet;

-- nominas en rangos de fechas
select * from paysheet p where p.created>='2018-01-21 09:12:00.391' and p.created<='2018-02-22 14:40:00'

select * from tax_movement;

select * from tax;

select * from price_movement;

-- obtengo la ultima nomina
select p.id AS "id nomina" , d.id_product , d.unit_price , d.quantity ,
	d.discount, t.iva ,t.cesc , t.discount ,t.subtotal as "subtotal taxes" ,
	p_m.percentage as "price range", p_m.total_price from paysheet	p 
	inner join paysheet_detail d on p.id = d.id_paysheet 
	inner join tax_movement t on d.id = t.id_paysheet_detail
	inner join price_movement p_m on d.id = p_m.id_paysheet_detail
	WHERE p.id = (SELECT id FROM paysheet ORDER BY id DESC LIMIT 1);

-- verificar si es la ultima nomina en base a la fecha
SELECT CASE WHEN (count(p)<1) THEN TRUE ELSE FALSE END from paysheet p WHERE p.created>'2028-02-22 14:38:00'
	
				