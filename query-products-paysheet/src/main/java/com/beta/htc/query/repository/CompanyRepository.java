package com.beta.htc.query.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.beta.htc.query.entity.Company;


public interface CompanyRepository extends JpaRepository<Company, Integer>{
}
