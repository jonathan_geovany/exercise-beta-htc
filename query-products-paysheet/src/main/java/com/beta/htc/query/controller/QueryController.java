package com.beta.htc.query.controller;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.beta.htc.query.entity.Paysheet;
import com.beta.htc.query.entity.Product;
import com.beta.htc.query.repository.PaysheetRepository;
import com.beta.htc.query.repository.ProductRepository;
import com.beta.htc.query.view.PaysheetView;
import com.beta.htc.query.view.ProductView;
import com.fasterxml.jackson.annotation.JsonView;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;


@RestController
public class QueryController {
	
	private static final String ENCONTRADO = "Encontrado";
	private static final String NO_ENCONTRADO = "No encontrado";
	private static final String DESCRIPTION = "Description";
	@Autowired
	private PaysheetRepository paysheetRepository;
	@Autowired
	private ProductRepository productRepository;
	
	@HystrixCommand
	@GetMapping("/paysheet/id/{id}")
	@JsonView(PaysheetView.class)
	public ResponseEntity<Paysheet> findPaysheetById(@PathVariable int id){
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status;
		Optional<Paysheet> searched  = paysheetRepository.findById(id);
		Paysheet paysheet;
		if(!searched.isPresent()) {
			headers.add(DESCRIPTION, NO_ENCONTRADO);
			status = HttpStatus.NOT_FOUND;
			paysheet = null;
		}else {
			headers.add(DESCRIPTION, ENCONTRADO);
			status = HttpStatus.OK;
			paysheet = searched.get();
		}
		return new ResponseEntity<>(paysheet, headers, status);
	}
	
	@HystrixCommand
	@GetMapping("/paysheet/date/{date}")
	@JsonView(PaysheetView.class)
	public ResponseEntity<Paysheet> findPaysheetByDate(@PathVariable Timestamp date){
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status;
		Optional<Paysheet> searched  = paysheetRepository.findByCreated(date);
		Paysheet paysheet;
		if(!searched.isPresent()) {
			headers.add(DESCRIPTION, NO_ENCONTRADO);
			status = HttpStatus.NOT_FOUND;
			paysheet = null;
		}else {
			headers.add(DESCRIPTION, ENCONTRADO);
			status = HttpStatus.OK;
			paysheet = searched.get();
		}
		return new ResponseEntity<>(paysheet, headers, status);
	}
	
	@HystrixCommand
	@GetMapping("/paysheet/from/{begin}/to/{end}")
	public ResponseEntity<List<Paysheet>> findPaysheetByDate(@PathVariable Timestamp begin,@PathVariable Timestamp end){
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status;
		List<Paysheet> list = paysheetRepository.findAllInRange(begin, end);
		if(list.isEmpty()) {
			headers.add(DESCRIPTION, NO_ENCONTRADO);
			status = HttpStatus.NOT_FOUND;
		}else {
			headers.add(DESCRIPTION, ENCONTRADO);
			status = HttpStatus.OK;
		}
		return new ResponseEntity<>(list, headers, status);
	}
	
	@HystrixCommand
	@GetMapping("/product/id/{id}")
	@JsonView(ProductView.class)
	public ResponseEntity<Product> findProductById(@PathVariable int id){
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status;
		Optional<Product> searched  = productRepository.findById(id);
		Product product;
		if(!searched.isPresent()) {
			headers.add(DESCRIPTION, NO_ENCONTRADO);
			status = HttpStatus.NOT_FOUND;
			product = null;
		}else {
			headers.add(DESCRIPTION, ENCONTRADO);
			status = HttpStatus.OK;
			product = searched.get();
		}
		return new ResponseEntity<>(product, headers, status);
	}
	
	
	@HystrixCommand
	@GetMapping("/product/code/{code}")
	@JsonView(ProductView.class)
	public ResponseEntity<Product> findProductByCode(@PathVariable String code){
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status;
		Optional<Product> searched  = productRepository.findByCode(code);
		Product product;
		if(!searched.isPresent()) {
			headers.add(DESCRIPTION, NO_ENCONTRADO);
			status = HttpStatus.NOT_FOUND;
			product = null;
		}else {
			headers.add(DESCRIPTION, ENCONTRADO);
			status = HttpStatus.OK;
			product = searched.get();
		}
		return new ResponseEntity<>(product, headers, status);
	}
	
	@HystrixCommand
	@GetMapping("/test")
	public String get() {
		return "hello world";
	}
}
