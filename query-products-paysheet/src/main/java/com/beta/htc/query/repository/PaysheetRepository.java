package com.beta.htc.query.repository;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.beta.htc.query.entity.Paysheet;


public interface PaysheetRepository extends JpaRepository<Paysheet, Integer>{
	public Optional<Paysheet> findByCreated(Timestamp time);
	
	@Query(nativeQuery=true,value="SELECT CASE WHEN COUNT(p)<1 THEN true ELSE false END from paysheet p WHERE p.created>?1")
	public Boolean isTheLastPaysheet(Timestamp inserted);
	
	@Query(nativeQuery=true,value="select * from paysheet p where p.created BETWEEN ?1 AND ?2")
	public List<Paysheet> findAllInRange(Timestamp beginDate,Timestamp endDate);
}
