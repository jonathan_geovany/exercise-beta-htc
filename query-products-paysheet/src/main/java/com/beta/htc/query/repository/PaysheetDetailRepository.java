package com.beta.htc.query.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.beta.htc.query.entity.PaysheetDetail;


public interface PaysheetDetailRepository extends JpaRepository<PaysheetDetail, Integer>{

}
