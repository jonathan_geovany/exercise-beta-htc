package com.beta.htc.query.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.beta.htc.query.entity.ProductPrice;


public interface ProductPriceRepository extends JpaRepository<ProductPrice, Integer> {

}
