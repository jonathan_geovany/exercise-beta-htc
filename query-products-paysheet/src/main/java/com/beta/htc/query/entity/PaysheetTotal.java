package com.beta.htc.query.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.beta.htc.query.view.PaysheetView;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;


@Entity
@Table(name = "paysheet_total")
public class PaysheetTotal implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 128953713847876179L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	@JsonView(PaysheetView.class)
	private Integer id;
	@ManyToOne
	@JoinColumn(name="id_paysheet")
	@JsonIgnore
	private Paysheet paysheet;
	@OneToOne
    @JoinColumn(name = "id_price_range", referencedColumnName = "id")
	@JsonIgnore
	private PriceRange priceRange;
	@Column(name = "total_price")
	@JsonView(PaysheetView.class)
	private BigDecimal totalPrice;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Paysheet getPaysheet() {
		return paysheet;
	}
	public void setPaysheet(Paysheet paysheet) {
		this.paysheet = paysheet;
	}
	public PriceRange getPriceRange() {
		return priceRange;
	}
	public void setPriceRange(PriceRange priceRange) {
		this.priceRange = priceRange;
	}
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	
}
