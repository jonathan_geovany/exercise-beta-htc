package com.beta.htc.query.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.beta.htc.query.view.ProductView;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;


@Entity
@Table(name = "product_price")
public class ProductPrice implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2601251315208236974L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	@JsonView(ProductView.class)
	private Integer id;
	@ManyToOne
	@JoinColumn(name="id_product", referencedColumnName = "id")
	@JsonIgnore
	private Product product;
	@OneToOne
    @JoinColumn(name = "id_price_range", referencedColumnName = "id")
	@JsonView(ProductView.class)
	private PriceRange priceRange;
	@Column(name = "total_price")
	@JsonView(ProductView.class)
	private BigDecimal totalPrice;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public PriceRange getPriceRange() {
		return priceRange;
	}
	public void setPriceRange(PriceRange priceRange) {
		this.priceRange = priceRange;
	}
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProductPrice [");
		if (id != null) {
			builder.append("id=");
			builder.append(id);
			builder.append(", ");
		}
		if (totalPrice != null) {
			builder.append("totalPrice=");
			builder.append(totalPrice);
		}
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
