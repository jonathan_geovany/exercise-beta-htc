package com.beta.htc.query.repository;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.beta.htc.query.entity.Product;


public interface ProductRepository extends JpaRepository<Product, Integer> {
	public Optional<Product> findByCode(String code);
}
