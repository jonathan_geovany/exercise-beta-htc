package com.beta.htc.query.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.beta.htc.query.entity.TaxMovement;


public interface TaxMovementRepository extends JpaRepository<TaxMovement, Integer> {

}
