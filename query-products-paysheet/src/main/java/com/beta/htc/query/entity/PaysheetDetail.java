package com.beta.htc.query.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.beta.htc.query.view.PaysheetView;
import com.beta.htc.query.view.ProductView;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;


@Entity
@Table(name = "paysheet_detail")
public class PaysheetDetail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1904995178790625475L;

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	@JsonView({PaysheetView.class,ProductView.class})
	private Integer id;
	@ManyToOne
	@JoinColumn(name="id_paysheet")
	@JsonIgnore
	private Paysheet paysheet;
	@ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_product",updatable=true)
	@JsonView(PaysheetView.class)
	private Product product;
	@Column(name = "unit_price")
	@JsonView({PaysheetView.class,ProductView.class})
	private BigDecimal unitPrice;
	@Column(name = "quantity")
	@JsonView({PaysheetView.class,ProductView.class})
	private Integer quantity;
	@Column(name = "exempt")
	@JsonView({PaysheetView.class,ProductView.class})
	private Boolean exempt;
	@Column(name = "cesc")
	@JsonView({PaysheetView.class,ProductView.class})
	private Boolean cesc;
	@Column(name = "discount")
	@JsonView({PaysheetView.class,ProductView.class})
	private BigDecimal discount;
	//mappedBy debe tener el mismo nombre de la variable donde se utiliza al otro lado
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy="paysheetDetail")
	@JsonView(PaysheetView.class)
	private List<PriceMovement> prices;
	@OneToOne(mappedBy = "paysheetDetail",cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	@JsonView(PaysheetView.class)
	private TaxMovement taxMovement;
	
	public List<PriceMovement> getPrices() {
		return prices;
	}
	public void setPrices(List<PriceMovement> prices) {
		this.prices = prices;
		this.prices.forEach(p -> p.setPaysheetDetail(this));
	}
	public TaxMovement getTaxMovement() {
		return taxMovement;
	}
	public void setTaxMovement(TaxMovement taxMovement) {
		this.taxMovement = taxMovement;
		this.taxMovement.setPaysheetDetail(this);
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Paysheet getPaysheet() {
		return paysheet;
	}
	public void setPaysheet(Paysheet paysheet) {
		this.paysheet = paysheet;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
		
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Boolean getExempt() {
		return exempt;
	}
	public void setExempt(Boolean exempt) {
		this.exempt = exempt;
	}
	public Boolean getCesc() {
		return cesc;
	}
	public void setCesc(Boolean cesc) {
		this.cesc = cesc;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public BigDecimal getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PaysheetDetail [id=");
		builder.append(id);
		builder.append(", product=");
		builder.append(product);
		builder.append(", unitPrice=");
		builder.append(unitPrice);
		builder.append(", quantity=");
		builder.append(quantity);
		builder.append(", exempt=");
		builder.append(exempt);
		builder.append(", cesc=");
		builder.append(cesc);
		builder.append(", discount=");
		builder.append(discount);
		builder.append("]");
		return builder.toString();
	}
	
	
	
	
	
}
