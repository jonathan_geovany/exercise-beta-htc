package com.beta.htc.query.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.beta.htc.query.view.PaysheetView;
import com.beta.htc.query.view.ProductView;
import com.fasterxml.jackson.annotation.JsonView;


@Entity
@Table(name = "price_range")
public class PriceRange implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9199336023231311123L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	@JsonView({PaysheetView.class,ProductView.class})
	private Integer id;
	@Column(name = "description")
	@JsonView({PaysheetView.class,ProductView.class})
	private String description;
	@Column(name = "percentage")
	@JsonView({PaysheetView.class,ProductView.class})
	private BigDecimal percentage;
	@Column(name = "enable")
	@JsonView({PaysheetView.class,ProductView.class})
	private Boolean enable;
	//relacion uno a uno con product price
	
	//relacion uno a uno con paysheet total
	
	//relacion uno a uno con 
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public BigDecimal getPercentage() {
		return percentage;
	}
	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}
	public Boolean getEnable() {
		return enable;
	}
	public void setEnable(Boolean enable) {
		this.enable = enable;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PriceRange [id=");
		builder.append(id);
		builder.append(", description=");
		builder.append(description);
		builder.append(", percentage=");
		builder.append(percentage);
		builder.append(", enable=");
		builder.append(enable);
		builder.append("]");
		return builder.toString();
	}
	
	

}
