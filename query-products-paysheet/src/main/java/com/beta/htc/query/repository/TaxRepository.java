package com.beta.htc.query.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.beta.htc.query.entity.Tax;


public interface TaxRepository extends JpaRepository<Tax, Integer>{
	public Tax findByDescription(String description);
}
