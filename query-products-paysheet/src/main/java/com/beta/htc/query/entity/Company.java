package com.beta.htc.query.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.beta.htc.query.view.PaysheetView;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;


@Entity
@Table(name = "company")
public class Company implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3701096011992378919L;
	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	@JsonView(PaysheetView.class)
	private Integer id;
	@Column(name = "name")
	@JsonView(PaysheetView.class)
	private String name;
	@Column(name = "sector")
	@JsonView(PaysheetView.class)
	private String sector;
	@Column(name = "nit")
	@JsonView(PaysheetView.class)
	private String nit;
	@Column(name = "address")
	@JsonView(PaysheetView.class)
	private String address;
	@Column(name = "phone")
	@JsonView(PaysheetView.class)
	private String phone;
	@OneToMany(mappedBy="company",fetch=FetchType.LAZY)
	@JsonIgnore
    private List<Paysheet> paysheets;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSector() {
		return sector;
	}
	public void setSector(String sector) {
		this.sector = sector;
	}
	public String getNit() {
		return nit;
	}
	public void setNit(String nit) {
		this.nit = nit;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public List<Paysheet> getPaysheets() {
		return paysheets;
	}
	public void setPaysheets(List<Paysheet> paysheets) {
		this.paysheets = paysheets;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Company [id=");
		builder.append(id);
		builder.append(", name=");
		builder.append(name);
		builder.append(", sector=");
		builder.append(sector);
		builder.append(", nit=");
		builder.append(nit);
		builder.append(", address=");
		builder.append(address);
		builder.append(", phone=");
		builder.append(phone);
		builder.append("]");
		return builder.toString();
	}
	
	
	
	
}
