package com.beta.htc.query.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.beta.htc.query.view.PaysheetView;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;


@Entity
@Table(name = "price_movement")
public class PriceMovement implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -8621832824555874590L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	@JsonView(PaysheetView.class)
	private Integer id;
	@OneToOne
    @JoinColumn(name = "id_price_range", referencedColumnName = "id")
	@JsonView(PaysheetView.class)
	private PriceRange priceRange;
	@ManyToOne
    @JoinColumn(name = "id_paysheet_detail")
	@JsonIgnore
	private PaysheetDetail paysheetDetail;
	@Column(name = "percentage")
	@JsonView(PaysheetView.class)
	private BigDecimal percentage;
	@Column(name = "total_price")
	@JsonView(PaysheetView.class)
	private BigDecimal totalPrice;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public PriceRange getPriceRange() {
		return priceRange;
	}
	public void setPriceRange(PriceRange priceRange) {
		this.priceRange = priceRange;
	}
	public PaysheetDetail getPaysheetDetail() {
		return paysheetDetail;
	}
	public void setPaysheetDetail(PaysheetDetail paysheetDetail) {
		this.paysheetDetail = paysheetDetail;
	}
	public BigDecimal getPercentage() {
		return percentage;
	}
	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	
	
}
